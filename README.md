# Demo 

Pour le test de nos modeles, désolé pas de démo... (les modèles sont un peu long à faire tourner!)
Cependant, nous avons crée un notebook interactif qui vous permet de suivre de A à Z un processus de deep learning pour
la segmentation en utilisant un U-net! Le fichier *Demo_Unet.ipynb* est un notebook jupyter et pour l'explorer il faut:
- telecharger le jeu de données (train seulement) [*GM Spinal Cord Challenge Dataset*](http://cmictig.cs.ucl.ac.uk/niftyweb/challenge/ )
- upload le jeu de données sur son drive Google
- télécharger Demo_Unet.ipynb et ouvrir le notebook avec Google Colab
- mount son drive (cf. dans le notebook)
- have fun!
